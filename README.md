Elaborado por: Nayara Valadares Santos
Data: 06/12/2016

Cenário: Iniciar uma nova conversa
Dado que estou no "WhatsApp"
Quando eu pressionar "Nova Conversa"
E eu pressionar "Buscar" para procurar um contato "Contato"
E eu seleciono o contato "contato" na lista
E eu pressionar o "campo de texto" 
Então eu pressiono "Enviar"


Cenário: Criar um grupo
Dado que estou no "WhatsApp"
Quando eu pressionar "Novo Grupo"
E eu selecionar os contatos "Contato1" e "contato2" e "contato3"
E eu pressionar "Seguinte"
E eu digitar o "nome do grupo"
Então eu pressiono "Criar"